package ru.lilitweb.authapi.repository;

import org.springframework.data.repository.CrudRepository;
import ru.lilitweb.authapi.domain.account.Account;

import java.util.Optional;
import java.util.UUID;

public interface AccountRepository extends CrudRepository<Account, UUID> {
    Optional<Account> findByLogin(String login);
}

