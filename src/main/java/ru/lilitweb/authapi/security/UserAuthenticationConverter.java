package ru.lilitweb.authapi.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.stereotype.Component;
import ru.lilitweb.authapi.domain.account.Account;

import java.util.Map;

@Component
public class UserAuthenticationConverter extends DefaultUserAuthenticationConverter {
    private final AccountUserDetailsManager userDetailsService;

    final String USER_ID = "user_id";

    public UserAuthenticationConverter(AccountUserDetailsManager userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public Map<String, ?> convertUserAuthentication(Authentication authentication) {
        //noinspection unchecked
        Map<String, Object> map = (Map<String, Object>) super.convertUserAuthentication(authentication);
        Account account = (Account) userDetailsService.loadUserByUsername(authentication.getName());
        map.put(USER_ID, account.getId());
        return map;
    }
}
