package ru.lilitweb.authapi.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;

@EnableConfigurationProperties(JwkSecurityProperties.class)
@Configuration
public class JwkKeyPairConfiguration {
    private final JwkSecurityProperties securityProperties;

    public JwkKeyPairConfiguration(JwkSecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Bean
    public KeyPair keyPair() {
        JwkSecurityProperties.JwtProperties jwtProperties = securityProperties.getJwt();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(jwtProperties.getKeyStore(), jwtProperties.getKeyStorePassword().toCharArray());
        return keyStoreKeyFactory.getKeyPair(jwtProperties.getKeyPairAlias(), jwtProperties.getKeyPairPassword().toCharArray());
    }
}
