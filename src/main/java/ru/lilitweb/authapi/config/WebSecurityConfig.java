package ru.lilitweb.authapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                    .csrf().disable()
                    .cors().disable()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .and()
                    .authorizeRequests()
                    .antMatchers(
                            "/",
                            "/signup",
                            "/webjars/**",
                            "/error**",
                            "/.well-known/jwks.json"
                    ).permitAll()
                .and()
                    .authorizeRequests()
                    .anyRequest().authenticated();
    }
}
