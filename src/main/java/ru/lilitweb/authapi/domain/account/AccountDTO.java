package ru.lilitweb.authapi.domain.account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
    UUID id;
    private String login;

    @Builder.Default
    private List<String> roles = new ArrayList<>();
}
