package ru.lilitweb.authapi.domain.account;


public class AccountAlreadyExistException extends RuntimeException {
    /**
     * Constructs a new runtime exception with login
     * @param login string
     */
    public AccountAlreadyExistException(String login) {
        super(String.format("User %s already exist", login));
    }
}
