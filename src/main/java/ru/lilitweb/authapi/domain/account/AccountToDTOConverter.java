package ru.lilitweb.authapi.domain.account;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountToDTOConverter implements Converter<Account, AccountDTO> {
    @Override
    public AccountDTO convert(Account source) {
        return AccountDTO.builder()
                .id(source.getId())
                .login(source.getLogin())
                .roles(source.getRoles())
                .build();
    }
}
