package ru.lilitweb.authapi.domain.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.lilitweb.authapi.repository.AccountRepository;

import java.util.Arrays;

@Component
public class AccountService {
    private PasswordEncoder passwordEncoder;
    private AccountRepository accounts;
    private AccountToDTOConverter converter;

    @Autowired
    public AccountService(PasswordEncoder passwordEncoder, AccountRepository accounts, AccountToDTOConverter converter) {
        this.passwordEncoder = passwordEncoder;
        this.accounts = accounts;
        this.converter = converter;
    }

    public AccountDTO signUp(String login, String password) {
        if (accounts.findByLogin(login).isPresent()) {
            throw new AccountAlreadyExistException(login);
        }

        Account account = accounts.save(Account.builder()
                .login(login)
                .password(this.passwordEncoder.encode(password))
                .roles(Arrays.asList("ROLE_USER", "ROLE_ADMIN"))
                .build()
        );

        return converter.convert(account);
    }
}
