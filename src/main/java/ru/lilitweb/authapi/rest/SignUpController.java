package ru.lilitweb.authapi.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.lilitweb.authapi.domain.account.AccountService;
import ru.lilitweb.authapi.domain.account.AccountDTO;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class SignUpController {

    AccountService accountService;

    @Autowired
    public SignUpController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/signup")
    public ResponseEntity<AccountDTO> signUp(@RequestBody @Valid SignUpRequest request) {
        String login = request.getLogin();
        String password = request.getPassword();

        return ok(accountService.signUp(login, password));
    }
}
